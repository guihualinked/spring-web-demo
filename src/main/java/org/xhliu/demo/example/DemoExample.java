package org.xhliu.demo.example;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.xhliu.demo.domain.od.common.Animal;
import org.xhliu.demo.domain.od.entity.Mouse;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author lxh
 * @date 2022/6/18-下午9:47
 */
public class DemoExample {
    public static void main(String[] args) {
        File file = new File("src/main/resources/body/BirdBody.json");
        try (
                InputStream in = new FileInputStream(file)
        ) {
            Mouse mouse = new Mouse();
            mouse.setMouseId("365ace546bea65efcec57f5fac22ba2f");
            mouse.setMouseType("pet");
            mouse.setCoatColor(0);
            mouse.setArea("Asia");
            mouse.setTailLength(10.2);

            ObjectMapper mapper = new ObjectMapper();

            String mouseStr = mapper.writeValueAsString(mouse);
            System.out.println("json = " + mouseStr);

//            List<Animal> animal = mapper.readValue(file, new TypeReference<>() {});
//            System.out.println(animal);
            Animal animal = mapper.readValue(mouseStr, Animal.class);
            System.out.println(animal);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
