package org.xhliu.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.xhliu.demo.domain.od.common.Animal;
import org.xhliu.demo.domain.od.entity.Mouse;

@SpringBootApplication
public class SpringWebDemoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(SpringWebDemoApplication.class, args);
    }

}
