package org.xhliu.demo.domain.od.common;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.xhliu.demo.domain.od.entity.Bird;
import org.xhliu.demo.domain.od.entity.Fish;
import org.xhliu.demo.domain.od.entity.Mouse;

/**
 * mark entity is animal class
 *
 * @author lxh
 * @date 2022/6/18-下午8:07
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Mouse.class, name = "Mouse"),
        @JsonSubTypes.Type(value = Fish.class, name = "Fish"),
        @JsonSubTypes.Type(value = Bird.class, name = "Bird")
})
public interface Animal {
}
